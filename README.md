# Installation Guide!
## Please ensure you take a complete backup of the site before doing this, as you will be directly modifying the sites files!

## Assets (Images, CSS Stylesheets, etc.):
- Login to SFTP on the server
- Upload the "plugins_public" folder directly to the sites root folder, not the systems!
- Log out of FTP.

## Index Page (Homepage):
- Login to UNA Studio, from UNA Studio's landing page, click pages.
![Alt Text](https://www.awesomescreenshot.com/upload//1079301/ba753359-19a2-47e1-757c-3551cba296a0.png)

- On Pages, click the dropdown, and then select Homepage.
![Alt Text](https://drive.google.com/uc?export=download&id=1g0OQoeDnQpVDkEvjEcqe14QNkzPNP-rh)

- Either delete, or set all the elements on the page to 'hidden'
- Press Add Block, and add a new 'raw' block.
- Copy and Paste the entirety of the contents of index.html into this block.
- Save!

## Signed-in (Unclear where this goes.)

- Install into the relevant page, using the same method.
- Clear all blocks or set as hidden
- Insert entirety of file as raw block.

## Sign-in pages and login pages.

### Signin Page:

- Place this HTML code into a 'raw' block

```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/plugins_public/homepage_assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/plugins_public/homepage_assets/fonts/stylesheet.css" />
<link rel="shortcut icon" href="/plugins_public/homepage_assets/images/favicon.png" />
<link rel="stylesheet" href="/plugins_public/homepage_assets/css/style.css" />
<title>Kiku Login</title>
</head>
<body class="inner-page-signin">
<header>
  <nav class="navbar navbar-expand-md navbar-dark">
    <a class="navbar-brand" href="/">
      <img src="/plugins_public/homepage_assets/images/logo.png" />
    </a>
  </nav>
</header>
```

- Add a Login form block
![Alt Text](https://drive.google.com/uc?export=download&id=1R4QVprmmRQbYvW9jXPYTEV--F3AMVuUa)

- Place this HTML code into a 'raw' block at the bottom.
```html
<script src="/plugins_public/homepage_assets/js/jquery-3.js"></script>
<script src="/plugins_public/homepage_assets/js/popper.min.js"></script>
<script type="text/javascript" src="/plugins_public/homepage_assets/js/bootstrap.min.js"></script>
</body>
</html>
```

## Signup page:
- Place this HTML code into a 'raw' block

```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/plugins_public/homepage_assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/plugins_public/homepage_assets/fonts/stylesheet.css" />
<link rel="shortcut icon" href="/plugins_public/homepage_assets/images/favicon.png" />
<link rel="stylesheet" href="/plugins_public/homepage_assets/css/style.css" />
<title>Kiku Sign Up</title>
</head>
<body class="inner-page-signup">
<header>
  <nav class="navbar navbar-expand-md navbar-dark"> <a class="navbar-brand" href="/"><img src="/plugins_public/homepage_assets/images/logo.png" /></a>
   <h2>KikuSongs</h2>
  </nav>
</header>
```

- Add a Signup form block using the method shown earlier.

- Place this HTML code into a 'raw' block at the bottom.
```html
<script src="/plugins_public/homepage_assets/js/jquery-3.js"></script>
<script src="/plugins_public/homepage_assets/js/popper.min.js"></script>
<script type="text/javascript" src="/plugins_public/homepage_assets/js/bootstrap.min.js"></script>
</body>
</html>
```
